package com.hywea.efusion.dao.db1;

import com.hywea.efusion.bean.AdminUser;

import java.util.List;

public interface AdminUser1Mapper {

    List<AdminUser> selectByPrimaryKey();
}
