package com.hywea.efusion.dao.db2;

import com.hywea.efusion.bean.AdminUser;

import java.util.List;

public interface AdminUser2Mapper {

    List<AdminUser> selectByPrimaryKey();
}
