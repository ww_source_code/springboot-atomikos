package com.hywea.efusion.service.impl;

import com.hywea.efusion.bean.AdminUser;
import com.hywea.efusion.dao.db1.AdminUser1Mapper;
import com.hywea.efusion.dao.db2.AdminUser2Mapper;
import com.hywea.efusion.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.transaction.UserTransaction;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TestServiceImpl implements TestService {


    @Autowired
    private AdminUser2Mapper commonMapper2;
    @Autowired
    private AdminUser1Mapper commonMapper1;
    @Autowired
    TransactionManager transactionManager;

    @Transactional
    public  Map<String, Object> test() {
        System.out.println(transactionManager);
        JtaTransactionManager jtaTransactionManager = (JtaTransactionManager)transactionManager;
        System.out.println(jtaTransactionManager.getUserTransaction());
        UserTransaction userTransaction = jtaTransactionManager.getUserTransaction();

        List<AdminUser> adminUsers1 = commonMapper1.selectByPrimaryKey();
        List<AdminUser> adminUsers2 = commonMapper2.selectByPrimaryKey();
        Map<String, Object> map = new HashMap<>();
        map.put("数据源1",adminUsers1);
        map.put("数据源2",adminUsers2);
        return map;
    }
}
