package com.hywea.efusion;

import com.hywea.efusion.config.DBConfig1;
import com.hywea.efusion.config.DBConfig2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({DBConfig1.class, DBConfig2.class})
public class EfusionApplication {

    public static void main(String[] args) {
        SpringApplication.run(EfusionApplication.class, args);
    }

}
